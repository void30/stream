import os
import pickle
import socket

import cv2
import requests
from getmac import get_mac_address as get_mac


BASE_URL = 'https://primusgrad.herokuapp.com/api/'
PORT = 8080


def update_ip(data):
	response = requests.put(BASE_URL + "pi/updateip/", json=data)
	if response.status_code != 202:
		print("something went wrong, exiting..")
		exit(1)


def create_new_raspberry():
	try:
		mac = get_mac()
		ipv4 = os.popen('ip addr show wlp3s0').read().split("inet ")[1].split("/")[0]
	except:
		print("something went wrong, exiting..")
		exit(1)
	data = {
		"mac_address": mac,
		"VPN_ip": ipv4
	}

	response = requests.post(BASE_URL + "pi/", json=data)
	if response.status_code == 400:
		update_ip(data)


def serve_forever():
	capture = cv2.VideoCapture(0)

	server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	server_socket.bind(('', PORT))

	while True:
		server_socket.listen(1)
		print(f"Server is listening on port {PORT}")

		client_sock, addr = server_socket.accept()
		print(f"Connection from {addr}")

		while True:
			try:
				_, frame = capture.read()
				frame = cv2.flip(frame, 1)

				data = pickle.dumps(frame)

				size = len(data)
				client_sock.sendall(str(size).encode('utf-8'))

				start = client_sock.recv(1024)

				if start.decode('utf-8') == "start":
					client_sock.sendall(data)

			except:
				client_sock.close()
				capture.release()
				print("Client Disconnected")
				break


if __name__ == "__main__":
	create_new_raspberry()
	serve_forever()
